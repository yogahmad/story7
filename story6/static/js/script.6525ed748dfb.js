$("body").addClass(localStorage.getItem("theme"));

$(document).ready(function() {
    $("#dark_theme").click(function() {
        localStorage.setItem("theme", "dark-toggled");
        $("body").removeClass("light-toggled");
        $("body").addClass("dark-toggled");
    });
});

$(document).ready(function() {
    $("#light_theme").click(function() {
        localStorage.setItem("theme", "light-toggled");
        $("body").removeClass("dark-toggled");
        $("body").addClass("light-toggled");
    });
});

    
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
